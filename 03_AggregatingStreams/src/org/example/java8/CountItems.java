package org.example.java8;

import java.util.ArrayList;
import java.util.List;

public class CountItems {
	public static void main(String args[]){

		long millisecondsStart = System.currentTimeMillis();
		executeCount();
		long timeSpentInMilliseconds = System.currentTimeMillis() - millisecondsStart;
		System.out.println("Total execution time: " + timeSpentInMilliseconds);
	
	}

	private static void executeCount() {
		System.out.println("Creating list");
		List<String> strings = new ArrayList<>();
		for (int i = 0; i < 10000; i++) {
			strings.add("Item " + i);
		}
		
		long count = strings.stream().parallel().count();
		
		System.out.println("Count: " + count);
	}
	
}