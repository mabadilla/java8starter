package org.example.java8;

import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;

import org.example.java8.model.Person;

public class SumAndAverage {

	public static void main(String args[]){

		long millisecondsStart = System.currentTimeMillis();
		executeSumAndAverage();
		long timeSpentInMilliseconds = System.currentTimeMillis() - millisecondsStart;
		System.out.println("Total execution time: " + timeSpentInMilliseconds);
				
	}

	private static void executeSumAndAverage() {
		List<Person> people = new ArrayList<>();

		for(int i = 0; i < 10000; i++){
			people.add(new Person("Joe", 48));
			people.add(new Person("Mary", 30));
			people.add(new Person("Mike", 73));
		}
		
		//TODO: 1. show sum only
		int sum = people.stream().parallel()
				.mapToInt(p -> p.getAge())
				.sum();
		System.out.println("Total of ages: " + sum);
		
		
		//TODO: 2. show avg
//		OptionalDouble avg = people.stream(). //parallel().
//				mapToInt(p -> p.getAge())
//				.average();
//		
//		if (avg.isPresent()) {
//			System.out.println("Average: " + avg.getAsDouble());
//		} else {
//			System.out.println("Average not calculated");
//		}
	}
	
}