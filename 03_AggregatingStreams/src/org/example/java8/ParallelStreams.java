package org.example.java8;

import java.util.ArrayList;
import java.util.List;

public class ParallelStreams {
	public static void main(String args[]){
		long millisecondsStart = System.currentTimeMillis();
		executeStreams();
		long timeSpentInMilliseconds = System.currentTimeMillis() - millisecondsStart;
		System.out.println("Total execution time: " + timeSpentInMilliseconds);

	}

	private static void executeStreams() {
		System.out.println("Creating list");
		List<String> strings = new ArrayList<>();
		for (int i = 0; i < 10000; i++) {
			strings.add("Item " + i);
		}

		//sequential
//		strings.stream()
//			.forEach(str -> System.out.println(str));
		
		//parallel
		strings.stream().parallel()
			.forEach(str -> System.out.println(str));
	}

}