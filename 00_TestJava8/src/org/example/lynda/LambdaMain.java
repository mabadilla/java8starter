package org.example.lynda;

public class LambdaMain {

	public static void main(String[] args) {
		Runnable r = () -> System.out.println("Hello!");
		new Thread(r).start();
	}

}
